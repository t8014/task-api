<?php

namespace App\Models;

use App\Enums\TaskStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $casts = [
        'status'  => TaskStatus::class,
        'user_id' => 'int',
    ];

    protected $fillable = [
        'title',
        'description',
        'status',
        'user_id',
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
