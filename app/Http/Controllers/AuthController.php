<?php

namespace App\Http\Controllers;

use App\Facades\Responder;
use App\Http\Requests\SigninRequest;
use App\Http\Requests\SignupRequest;
use App\Http\Resources\UserResource;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JetBrains\PhpStorm\ArrayShape;

class AuthController extends Controller
{
    private UserRepository $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function signin(SigninRequest $request)
    {
        //check info user
        if (!Auth::attempt($request->only(['email', 'password'])))
            return Responder::validationError(['signin' => trans('auth.failed')]);
        //create token and return
        return $this->repository->token($this->repository->findByEmail($request->input('email')));
    }

    public function signup(SignupRequest $request)
    {
        //payload
        $data = $request->merge([
            'email_verified_at' => now(),
            'password'          => bcrypt($request->input('password')),
        ])->all();
        //create user
        $user = $this->repository->create($data);
        //create token and return
        return Responder::created($this->repository->token($user));
    }

    public function signout()
    {
        Auth::user()->currentAccessToken()->delete();
        return Responder::respond();
    }

    public function user(): \Illuminate\Http\Response
    {
        //get current user
        return Responder::respond(new UserResource(Auth::user()));
    }

    public function checkUserExistsByEmail($email): \Illuminate\Http\Response
    {
        $check = $this->repository->existsEmail($email);
        return Responder::respond($check);
    }


}
