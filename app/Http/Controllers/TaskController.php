<?php

namespace App\Http\Controllers;

use App\Enums\TaskStatus;
use App\Facades\Responder;
use App\Http\Requests\TaskRequest;
use App\Http\Resources\TaskResource;
use App\Repositories\TaskRepository;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    use BaseControllerTrait;

    public function __construct(TaskRepository $repository)
    {
        $this->repository = $repository;
        //load user
        $this->repository->with(['user']);

        $this->request = TaskRequest::class;
        $this->resource = TaskResource::class;
    }

    public function index()
    {
        $this->repository->where('user_id', auth()->id());
        if ($this->paginate){
            $data = $this->repository->paginate();
            $data = [
                'data'         => $this->resource::collection($data->items()),
                'per_page'     => $data->perPage(),
                'total'        => $data->total(),
                'current_page' => $data->currentPage(),
            ];
        }
        else {
            $data = $this->resource::collection($this->repository->all());
        }

        return Responder::respond($data);
    }

    /**
     * Show the specified resource.
     * @param $modelId
     * @return mixed
     */
    public function show($modelId)
    {
        $data = $this->repository->find($modelId);
        $this->authorize('view', $data);
        return Responder::respond(new $this->resource($data));
    }


    public function update($modelId, Request $request)
    {
        $data = $this->repository->find($modelId);
        $this->authorize('update', $data);
        if (isset($this->request)){
            app($this->request);
        }

        $data->update($request->toArray());

        return $data ? Responder::updated(new $this->resource($data)) : Responder::badRequest();
    }

    public function destroy($modelId, Request $request)
    {
        $data = $this->repository->find($modelId);
        $this->authorize('delete', $data);

        return $data->delete() ? Responder::deleted() : Responder::badRequest();
    }

    public function toggle($modelId)
    {
        //get task
        $data = $this->repository->find($modelId);

        //check status and update
        if ($data->status == TaskStatus::Pending)
            $data->update(['status' => TaskStatus::Completed]);
        else if ($data->status == TaskStatus::Completed)
            $data->update(['status' => TaskStatus::Pending]);

        return Responder::respond($data);
    }
}
