<?php

namespace Tests\Feature\App\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use WithFaker;

    public function test_signup_ok()
    {
        $password = $this->faker->password;
        $res = $this->post(route('signup'), [
            'name'                  => $this->faker->name,
            'email'                 => $this->faker->safeEmail,
            'password'              => $password,
            'password_confirmation' => $password,
        ]);
        $res->assertCreated()->assertJsonStructure(['token']);
    }

    public function test_signup_validation()
    {
        $res = $this->post(route('signup'));
        $res->assertInvalid();
    }

    public function test_signin_ok()
    {
        $user = User::factory()->create();
        $res = $this->post(route('signin'), [
            'email'    => $user->email,
            'password' => 'password',
        ]);
        $res->assertOk()->assertJsonStructure(['token']);
    }

    public function test_signin_not_match_data()
    {
        $user = User::factory()->create();
        $res = $this->post(route('signin'), [
            'email'    => $user->email,
            'password' => $user->id,
        ]);
        $res->assertStatus(302);
    }

    public function test_signin_validation()
    {
        $res = $this->post(route('signin'));
        $res->assertInvalid();
    }

    public function test_get_user_authorized()
    {
        Sanctum::actingAs(User::factory()->create(), ['*']);

        $res = $this->get(route('user'));

        $res->assertOk()->assertJsonStructure(['id', 'name', 'email']);
    }

    public function test_get_user_unauthorized()
    {
        $res = $this->getJson(route('user'));

        $res->assertUnauthorized();
    }
}
