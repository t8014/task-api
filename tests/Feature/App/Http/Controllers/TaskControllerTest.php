<?php

namespace Tests\Feature\App\Http\Controllers;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class TaskControllerTest extends TestCase
{
    use WithFaker;

    public function test_user_unauthorized()
    {
        $task = Task::factory()->create(['user_id' => 1]);
        $res = $this->getJson(route('task.show', ['task' => $task->id]));

        $res->assertUnauthorized();
    }

    public function test_user_forbidden()
    {
        Sanctum::actingAs($user = User::factory()->create(), ['*']);
        $task = Task::factory()->create(['user_id' => 1]);
        $res = $this->getJson(route('task.show', ['task' => $task->id]));

        $res->assertForbidden();
    }

    public function test_get_all()
    {
        Sanctum::actingAs(User::factory()->create(), ['*']);

        $res = $this->getJson(route('task.index'));

        $res->assertStatus(200);
    }

    public function test_get_view_current_user()
    {
        Sanctum::actingAs($user = User::factory()->create(), ['*']);
        $task = Task::factory()->create(['user_id' => $user->id]);
        $res = $this->getJson(route('task.show', ['task' => $task->id]));

        $res->assertStatus(200);
    }

    public function test_create()
    {
        Sanctum::actingAs($user = User::factory()->create(), ['*']);
        $res = $this->postJson(route('task.store'), [
            'title'       => $this->faker->name,
            'description' => $this->faker->text(100),
        ]);
        $res->assertCreated();
    }

    public function test_update()
    {
        Sanctum::actingAs($user = User::factory()->create(), ['*']);
        $task = Task::factory()->create(['user_id' => $user->id]);
        $res = $this->putJson(route('task.update', ['task' => $task->id]), [
            'title'       => $this->faker->name,
            'description' => $this->faker->text(100),
        ]);
        $res->assertOk();
    }

    public function test_delete()
    {
        Sanctum::actingAs($user = User::factory()->create(), ['*']);
        $task = Task::factory()->create(['user_id' => $user->id]);
        $res = $this->deleteJson(route('task.destroy', ['task' => $task->id]));
        $res->assertOk();
    }

    public function test_toggle()
    {
        Sanctum::actingAs($user = User::factory()->create(), ['*']);
        $task = Task::factory()->create(['user_id' => $user->id]);
        $res = $this->putJson(route('task.toggle', ['task' => $task->id]));
        $res->assertOk();
    }
}
